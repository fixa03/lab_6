﻿using System;
using System.Collections.Generic;

namespace lab_6
{
    class Program
    {

        struct field 
        {
            public string key { get; }
            public string value { get; }

            public field(string Key, string Value)
            {
                key = Key;
                value = Value;
            }
        };

        class HashTable 
        {
            private int Lenght;
            private int ActiveLenght = 0;
            private List<field>[] data;

            public HashTable(int lenght)
            {
                Lenght = lenght;
                data = new List<field>[lenght];

                for (int i = 0; i < lenght; i++)
                    data[i] = new List<field>();

            }

            public int GetLenght() { return Lenght; }

            public int HashCode(string key, int M)
            {
                int h = 1, a = 31415, b = 27183;
                
                //source
                //for (h = 0; *v != 0; v++, )
                //    h = (a * h + *v) % M;

                foreach (char c in key)
                {
                    a = a * b % (M - 1);
                    h = (a * h + c) % M;
                }
                
                return (h < 0) ? (h + M) : h;   
            }

            public void Add(string key, string value)
            {
                
                field temp = new field(key, value);
                data[HashCode(key, Lenght)].Add(temp);

                ActiveLenght += 1;
                if (ActiveLenght >= Math.Ceiling(Lenght / 2.0))
                    IncLenght(2 * Lenght);


            }

            public void IncLenght(int NewLenght)
            {
                List<field>[] copy = new List<field>[Lenght];
                for (int i = 0; i < copy.Length; i++)
                    copy[i] = new List<field>();
                data.CopyTo(copy, 0);

                Lenght = NewLenght;
                data = new List<field>[Lenght];
                for (int i = 0; i < data.Length; i++)
                    data[i] = new List<field>();

                //field[] temp_array;
                for (int i = 0; i < copy.Length; i++)
                {
                    //temp_array = new field[copy[i].Count];
                    foreach (field temp in copy[i])
                    {
                        data[HashCode(temp.key, Lenght)].Add(temp);
                    }
                
                }
            }

            public void Show()
            {
                for (int i = 0; i < data.Length; i++)
                {
                    Console.WriteLine("data[" + i + "]");
                    foreach (field temp in data[i])
                        Console.WriteLine("\t" + temp.key + "\t" + temp.value);
                    

                }
            }

            public void ShowTable()
            {
                Console.WriteLine("Длина: " + Lenght);
                Console.WriteLine("Количество элементов: " + ActiveLenght);

                Console.Write("Индекс\t");
                for(int i = 0; i < Lenght; i++) Console.Write(i + "\t");
                Console.WriteLine();
                Console.Write("Элемент\t");
                for (int i = 0; i < Lenght; i++) Console.Write(data[i].Count + "\t");
            }

            public void Search(string key)
            {
                foreach (field temp in data[HashCode(key, Lenght)])
                    if (temp.key == key)
                    {
                        Console.WriteLine(temp.value);
                        return;
                    }

                Console.WriteLine("Не найдено");
            }

            public void Delete(string key)
            {
                foreach (field temp in data[HashCode(key, Lenght)])
                    if (temp.key == key)
                    {
                        Console.WriteLine(temp.value);
                        data[HashCode(key, Lenght)].Remove(temp);
                        ActiveLenght -= 1;
                        return;
                    }

                Console.WriteLine("Элемент не найден");
            }

        };


        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            HashTable ht1 = new HashTable(5);

            int input = 0;
            do
            {
                Console.WriteLine("Команды: ");
                Console.WriteLine("1. Создать хэш таблицу");
                Console.WriteLine("2. Добавить элемент");
                Console.WriteLine("3. Удалить элемент");
                Console.WriteLine("4. Вывести слово по подсказки");
                Console.WriteLine("5. Увеличить хэш таблицу");
                Console.WriteLine("6. Вывести хэш таблицу");
                Console.WriteLine("AnyKey. Выход");
                Console.Write(">");
                input = Convert.ToInt32(Console.ReadLine());

                Console.Clear();

                switch (input)
                {
                    case 1:
                        Console.WriteLine("Введите длину хэш таблицы:");
                        int len = Convert.ToInt32(Console.ReadLine());
                        ht1 = new HashTable(len);
                        
                        break;

                    case 2:
                        Console.WriteLine("Введите подсказку и слово:");
                        string key = Console.ReadLine();
                        string value = Console.ReadLine();

                         ht1.Add(key, value);
                        break;
                    case 3:
                        Console.Write("Введите подсказку для удаления:");
                        string key_delete = Console.ReadLine();

                        ht1.Delete(key_delete);
                        break;
                    case 4:
                        Console.Write("Введите слово:");
                        string word = Console.ReadLine();
                        ht1.Search(word);
                        break;
                    case 5:
                        Console.Write("Введите длину:");
                        int new_len = Convert.ToInt32(Console.ReadLine());

                        ht1.IncLenght(new_len);
                        break;
                    case 6:
                        ht1.Show();
                        break;
                }

                ht1.ShowTable();

                Console.ReadLine(); Console.Clear();

            } while (input != 0);


            ht1.Add("text1", "text1");
            ht1.Add("text2", "text2");
            ht1.Add("text3", "text3");
            ht1.Add("text4", "text4");
            ht1.Add("hello", "world");
            ht1.Add("привет", "мир");
            ht1.Add("вода", "мокрая");
            ht1.Add("hello", "world");


            ht1.Show();

            ht1.Search("text15");
            ht1.GetLenght();
            ht1.Show();

        }
    }
}
